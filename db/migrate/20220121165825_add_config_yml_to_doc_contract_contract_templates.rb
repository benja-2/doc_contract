# frozen_string_literal: true
class AddConfigYmlToDocContractContractTemplates < ActiveRecord::Migration[7.0]
  def change
    add_column :doc_contract_contract_templates, :config_yml, :text
  end
end
