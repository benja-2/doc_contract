# frozen_string_literal: true
class CreateDocContractContractTemplates < ActiveRecord::Migration[7.0]
  def change
    create_table :doc_contract_contract_templates do |t|
      t.string :name
      t.string :latex_template

      t.timestamps
    end
  end
end
