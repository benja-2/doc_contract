# frozen_string_literal: true
class AddTitlepageBackgroundToContractTemplates < ActiveRecord::Migration[7.0]
  def change
    add_column :doc_contract_contract_templates, :titlepage_background, :string
  end
end
