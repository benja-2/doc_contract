# frozen_string_literal: true
class AddMarkdownToDocContractContractTemplates < ActiveRecord::Migration[7.0]
  def change
    add_column :doc_contract_contract_templates, :markdown, :text
  end
end
