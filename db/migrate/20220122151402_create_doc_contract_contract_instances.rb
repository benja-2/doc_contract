# frozen_string_literal: true
class CreateDocContractContractInstances < ActiveRecord::Migration[7.0]
  def change
    create_table :doc_contract_contract_instances do |t|
      t.string :name
      t.text :config_yml
      t.belongs_to :contract_template, null: false, foreign_key: {to_table: 'doc_contract_contract_templates'}

      t.timestamps
    end
  end
end
