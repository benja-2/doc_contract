# frozen_string_literal: true

module ActiveModel
  class Name

    def human_plural
      # Try to find the plural name of a model. If none can be found try to find a non namespaced version and return that one
      default = proc do |path|
        if path.present? and path['/'] # non namespaced model a.b.c/d/e => a.b.c
          unnamespaced_path = path.sub(%r{\.(\w+)/[\w\\]+}, '.\1')
          I18n.t(unnamespaced_path, default: proc{ human.pluralize })
        else
          human.pluralize
        end
      end
      I18n.t("#{@klass.i18n_scope}.models.plural.#{i18n_key}", default: default)
    end
  end
end
