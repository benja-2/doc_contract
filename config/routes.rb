# frozen_string_literal: true
DocContract::Engine.routes.draw do
  resources :contract_templates do
    member do
      post :processed_markdown
      post :processed_html
      get :pdf
    end
  end
  resources :contract_instances do
    member do
      post :processed_markdown
      post :processed_html
      get :pdf
    end
  end
  root 'application#main'
end
