# frozen_string_literal: true
require 'doc_contract/version'
require 'doc_contract/engine'
require 'doc_contract/handlebars'
require 'ruby-handlebars'
require 'open3'

module DocContract
  # Your code goes here...
end
