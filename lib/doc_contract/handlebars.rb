# frozen_string_literal: true

module DocContract
  class Handlebars

    COMPILE_ATTRIBUTES = %w[
      title subtitle
      header-left header-center header-right
      footer-left footer-center footer-right
    ].freeze

    class DummyEscaper

      def self.escape(value)
        value
      end
    end

    # rubocop:disable Metrics/MethodLength
    def self.renderer
      return @handlebars_renderer if @handlebars_renderer

      renderer = ::Handlebars::Handlebars.new
      renderer.set_escaper DummyEscaper # Do not escape HTML
      currency_helper = proc do |_context, value, currency_symbol|
        if value.is_a?(Integer) or value.to_i == value
          format("#{currency_symbol} %.0f,-", value) #.gsub(/(\d)(?=\d{3}+,)/, '\1.')
        else
          format("#{currency_symbol} %.2f", value.to_f) #.gsub(/(\d)(?=\d{3}+,)/, '\1.')
        end
      end
      #renderer.register_helper(:euro) do |context, value|
      #  if Integer === value or value.to_i == value
      #    sprintf("&euro; %.0f,-", value) #.gsub(/(\d)(?=\d{3}+,)/, '\1.')
      #  else
      #    sprintf("&euro; %.2f", value.to_f) #.gsub(/(\d)(?=\d{3}+,)/, '\1.')
      #  end
      #end
      eval_boolean = proc do |context, values, compare_lambda|
        blocks, comparables = values.partition{ |v| v.is_a? ::Handlebars::Tree::Block }
        true_blk, false_blk = blocks
        comparables = comparables.map{ |v| v.is_a?(Parslet::Slice) ? v.to_s : v }
        are_equal = if compare_lambda.arity == 1
                      compare_lambda.call(comparables)
                    else
                      compare_lambda.call(*comparables)
                    end
        if true_blk.items.present? # assumed internal helper usecase without blocks
          are_equal ? true_blk.fn(context) : false_blk.try(:fn, context)
        else
          are_equal
        end
      end
      renderer.register_helper(:euro) { |context, value| currency_helper.call context, value, '&euro;' }
      renderer.register_helper(:dollar) { |context, value| currency_helper.call context, value, '$' }
      renderer.register_helper(:peso) { |context, value| currency_helper.call context, value, '$' }
      renderer.register_helper(:plus) { |_context, value, addition, *_rest| value.to_f + addition.to_f }
      renderer.register_helper(:sum)  { |_context, *values, _bkl| (values.first.is_a?(Array) ? values.first : values).map(&:to_f).sum }
      renderer.register_helper(:eq)   { |context, *values| eval_boolean.call context, values, ->(vals){ vals.uniq.length <= 1 } }
      renderer.register_helper(:neq)  { |context, *values| eval_boolean.call context, values, ->(vals){ vals.uniq.length > 1 } }
      renderer.register_helper(:gt)   { |context, *values| eval_boolean.call context, values, ->(a, b){ a > b } }
      renderer.register_helper(:includes) { |context, *values| eval_boolean.call context, values, ->(vals){ vals.first.include? vals.last } }
      renderer.register_helper(:map) do |_context, array, key|
        return [] unless array.present?

        array.map{ |e| e[key.to_s] || e[key.to_sym] }
      end
      renderer.register_helper(:upcase) { |_context, value| value.to_s.upcase }
      renderer.register_helper(:downcase) { |_context, value| value.to_s.downcase }
      renderer.register_helper(:to_sentence) do |_context, array, nester|
        case nester
        when String, Parslet::Slice
          array.map{ |e| e[nester.to_s] || e[nester.to_sym] }.to_sentence
        else
          array.to_sentence
        end
      end
      renderer.register_helper(:to_words) { |_context, value| value.to_words }
      renderer.register_helper(:first_present) do |_context, *args|
        args.find(&:present?)
      end
      @handlebars_renderer = renderer
    end
    # rubocop:enable Metrics/MethodLength

    def self.compile(text, object, process_attributes: true)
      return '' unless text.present?

      object ||= {}
      if process_attributes
        object.stringify_keys!
        enrich_object(object)
      end
      compiled = renderer.compile(text)
      compiled.call(object)
    end

    def self.enrich_object(object)
      object['today'] = Date.today.iso8601 if object['today'].blank?
      iteration_keys = object.keys
      iteration_keys.each do |k|
        if k =~ /_items$/ and object[k].is_a?(Array)
          amounts_sum = object[k].map{ |o| (o[:amount] || o['amount']).to_f }.sum
          object["#{k}_total"] = amounts_sum
        end
        if COMPILE_ATTRIBUTES.include? k
          #TODO: check self reference, maybe even circular?
          object[k] = compile(object[k], object, process_attributes: false)
        end
      end
      object
    end
  end
end
