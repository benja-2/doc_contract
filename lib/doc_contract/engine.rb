# frozen_string_literal: true
require 'cancancan'
require 'jquery-rails'
require 'kaminari'
require 'numbers_and_words'
require 'ransack'
require 'redcarpet'

module DocContract
  class Engine < ::Rails::Engine

    #isolate_namespace self.name.deconstantize.safe_constantize
    isolate_namespace DocContract

    initializer 'doc_contract.assets.precompile' do |main_app|
      #app.config.assets.precompile << "config/engine_name_manifest.js"
      main_app.config.assets.precompile << 'doc_contract/application.css'
      main_app.config.assets.precompile << 'doc_contract/application.js'

      # Engine specific config
      # Allow setting false, so check for nil
      main_app.config.x.doc_contract.show_readme_on_main_page = true if main_app.config.x.doc_contract.show_readme_on_main_page.nil?
      main_app.config.x.doc_contract.application_title ||= -> { 'DOC-contract' }
      main_app.config.x.doc_contract.link_home_content ||= -> { '<i class="arrow left icon"></i> Back' }
    end

    # add migrations to containing application
    initializer 'doc_contract.append_migrations' do |app|
      unless app.root.to_s.match root.to_s
        config.paths['db/migrate'].expanded.each do |expanded_path|
          app.config.paths['db/migrate'] << expanded_path
        end
      end
    end

    initializer 'doc_contract.ensure_set_field_error_proc' do |app|
      app.config.action_view.field_error_proc ||= proc do |html_tag, _instance|
        %|<div class="field_with_errors field error has-error">#{html_tag}</div>|.html_safe
      end
    end
  end
end
