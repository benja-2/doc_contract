# frozen_string_literal: true

module DocContract
  module ApplicationHelper
    # Return active or nil based on the given route spec
    #   %li{ class: active_class('users') # true if controller is users, false otherwise
    #   %li{ class: active_class('pages#about') # true if controller is pages and action is about
    #NOTE: Taken from the dunlop-core gem. That is the best maintained version
    def active_class(*route_specs)
      options = route_specs.extract_options!
      return nil if Array.wrap(options[:except]).any?{ |exception| current_route_spec?(exception) }
      return 'active' if route_specs.any?{ |rs| current_route_spec?(rs, options) }

      nil
    end

    # Check if the current route matches the route given as argument.
    # The syntax is meant to be a bit similar to specifying routes in
    # `config/routes.rb`.
    #   current_route_spec?('products') #=> true if controller name is products, false otherwise
    #   current_route_spec?('products#show') #=> true if controller_name is products AND action_name is show
    #   current_route_spec?('#show') #=> true if action_name is show, false otherwise
    #NOTE: this helper is tested through the active_class helper
    #NOTE: Taken from the dunlop-core gem. That is the best maintained version
    def current_route_spec?(route_spec, _options = {})
      return route_spec.match([controller_path, action_name].join('#')) if route_spec.is_a?(Regexp)

      controller, action = route_spec.split('#')
      return action == params[:id] if controller_path == 'high_voltage/pages'

      actual_controller_parts = controller_path.split('/')
      if controller #and controller_path == controller
        tested_controller_parts = controller.split('/')
        return if tested_controller_parts.size > actual_controller_parts.size

        if actual_controller_parts[0...tested_controller_parts.size] == tested_controller_parts
          # controller spec matches
          return true unless action

          action_name == action
        end
      else
        action_name == action
      end
    end

    #NOTE: Taken from the dunlop-core gem. That is the best maintained version
    def page_title(*args, &blk)
      extra_content = capture(&blk) if blk.present?
      if resource_title?(args)
        @scope_model = args[1].is_a?(ActiveRecord::Base) ? args[1].class : args[1]
        content = page_title_for_resource(args)
      else
        content = page_title_for_string(args)
      end
      content += extra_content if extra_content.present?
      title_tag = content_tag(:h2, content, class: 'page-title ui header')
      content_for :page_title, title_tag
      title_tag
    end

    def resource_title?(args)
      args.first.is_a?(Symbol) &&
        (args[1].respond_to?(:model_name) || args[1].class.respond_to?(:model_name))
    end

    def page_title_for_string(args)
      title = html_escape(args.first)
      options = args.last.is_a?(Hash) ? args.last : {}
      if back_options = options[:back]
        url =
          case back_options
          when Array then polymorphic_path(back_options)
          when true then :back
          else back_options
          end
        if url
          back_link = link_to content_tag(:i, nil, class: 'left arrow icon'), url, class: 'title-back-link'
          title = back_link.safe_concat(title)
        end
      end
      title
    end

    def page_title_for_resource(args)
      options = args.extract_options!
      model = args[1].respond_to?(:model_name) ? args[1] : args[1].class
      title = if args.first == :index
                t('action.index.label', models: model.model_name.human_plural).html_safe
              else
                t("action.#{args.first}.label", model: model.model_name.human).html_safe
              end
      if back_options = options[:back]
        url =
          case back_options
          when Array then polymorphic_path(back_options)
          when true then :back
          else back_options
          end
        if url
          back_link = link_to content_tag(:i, nil, class: 'left arrow icon'), url, class: 'title-back-link'
          title = back_link.safe_concat(title)
        end
      end
      title
    end

    def search_result_info(records)
      from_item = (records.current_page - 1) * records.limit_value + 1
      to_item = [from_item + records.size - 1, records.total_count].min
      from_item = 0 if records.total_count.zero?
      "#{from_item} - #{to_item} / #{records.total_count}"
    end

    def at(attribute_name, scope_model = nil)
      scope_model ||= @scope_model
      scope_model.human_attribute_name(attribute_name)
    end

    # Search helpers from dunlop
    def search_row(options = {}, &blk)
      classes = Array.wrap(options[:class])
      classes |= ['search']
      classes << 'conditions-present' if @q.conditions.present?
      content = capture(&blk)
      content_tag(:tr, content, class: classes)
    end

    # This helper returns the link for showing a record inside a table
    def table_show_link(record, path = nil, options = {})
      if options.has_key?(:authorized)
        return unless options[:authorized]
      else
        return unless can? :show, record
      end
      link_to(
        content_tag(:i, nil, class: 'folder open icon'),
        path || record,
        class: 'table-link show ui mini basic primary icon button'
      )
    end

    # This helper returns the link for showing a record inside a table
    def table_download_link(record, path = nil, options = {})
      if options.has_key?(:authorized)
        return unless options[:authorized]
      else
        return unless can? :download, record
      end
      link_to(
        content_tag(:i, nil, class: 'download icon'),
        path || [:download, record],
        class: 'table-link download ui mini violet icon button'
      )
    end

    # This helper returns the link for editing a record inside a table
    def table_edit_link(record, path = nil, options = {})
      if options.has_key?(:authorized)
        return unless options[:authorized]
      else
        return unless can? :update, record
      end
      link_to(content_tag(:i, nil, class: 'write icon'), path || [:edit, record], class: 'table-link edit ui mini basic yellow icon button')
    end

    def table_destroy_link(record, path = nil, options = {})
      if options.has_key?(:authorized)
        return unless options[:authorized]
      else
        return unless can? :destroy, record
      end
      confirm_text = "Are you sure you want to delete #{record.class.model_name.human}"
      record_name = nil
      record_name = record.presentation_name if record.respond_to?(:presentation_name)
      record_name ||= record.name if record.respond_to?(:name)
      record_name ||= record.title if record.respond_to?(:title)
      confirm_text << " #{record_name}" if record_name.present?
      confirm_text << '?'
      link_to(
        content_tag(:i, nil, class: 'trash icon'),
        path || record,
        method: :delete,
        data: { confirm: confirm_text },
        class: 'table-link destroy ui mini negative icon button'
      )
    end

    # https://coderwall.com/p/7gqmog/display-flash-messages-with-semantic-ui-in-rails
    def flash_class(level)
      case level.to_sym
      when :success       then 'ui positive message'
      when :error, :alert then 'ui negative message'
      when :notice        then 'ui info message'
      else "ui #{level} message"
      end
    end
  end
end
