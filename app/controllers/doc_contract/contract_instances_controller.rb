# frozen_string_literal: true
module DocContract
  class ContractInstancesController < ApplicationController
    include SharedTemplateAndInstanceMethods
    helper_method :record_class
    respond_to :html

    def index
      authorize! :index, record_class
      @q = record_class.ransack query
      @q.sorts = 'name asc' if @q.sorts.empty?
      @records = @q.result.includes(:contract_template).page(params[:page]).per(100)
      respond_with(@records)
    end

    def new
      @record = record_class.new
      authorize! :create, @record
    end

    def create
      @record = record_class.new record_params
      authorize! :create, @record
      if @record.save
        redirect_to @record
      else
        render :new, status: :unprocessable_entity
      end
    end

    def show
      @record = record_class.find params[:id]
      authorize! :show, @record
    end

    def edit
      @record = record_class.find(params[:id])
      authorize! :edit, @record
    end

    def update
      @record = record_class.find params[:id]
      authorize! :edit, @record
      if @record.update record_params
        redirect_to @record
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @record = record_class.find params[:id]
      authorize! :destroy, @record
      @record.destroy
      redirect_to record_class
    end
  end
end
