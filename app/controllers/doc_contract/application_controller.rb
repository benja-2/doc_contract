# frozen_string_literal: true

module DocContract
  class ApplicationController < ::ApplicationController
    helper_method :query, :readme_html

    def main
      # root landing action
    end

    private

    def query
      params[:q]
    end

    def readme_html
      result = DocContract::Engine.root.join('README.md').read
      markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, tables: true, fenced_code_blocks: true)
      markdown.render result
    end

  end
end
