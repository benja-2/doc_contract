# frozen_string_literal: true
module DocContract
  module SharedTemplateAndInstanceMethods
    # POST /doc-contract/contract_templates/:id/processed_markdown?markdown=...
    def processed_markdown
      @record = record_class.find params[:id]
      authorize! :show, @record
      # Allow preview using the markdown parameter. Fallback to actual saved
      # version when param is not given
      @record.markdown = params[:markdown] if params[:markdown].present?
      render json: {markdown: @record.processed_markdown}
    end

    def processed_html
      @record = record_class.find params[:id]
      authorize! :show, @record
      # Allow preview using the markdown parameter. Fallback to actual saved
      # version when param is not given
      @record.markdown = params[:markdown] if params[:markdown].present?
      render json: {html: @record.processed_html}
    end

    # rubocop:disable Metrics/MethodLength
    # for now keep as a whole story
    def pdf
      @record = record_class.find params[:id]
      authorize! :show, @record

      # Allow preview using the markdown parameter. Fallback to actual saved
      # version when param is not given
      @record.markdown = params[:markdown] if params[:markdown].present?
      return redirect_to @record unless @record.markdown.present?

      @tempfile_markdown = Tempfile.new(['pandoc-markdown', '.md'])
      config_addittions = {}
      if nbackground = @record.try(:titlepage_background) || @record.contract_template.titlepage_background.presence
        config_addittions['titlepage-background'] = "vendor/assets/doc-contract/titlepage-backgrounds/background#{nbackground}.pdf"
      end
      template_markdown = @record.template_markdown(config_addittions)
      @tempfile_markdown.write template_markdown
      #@tempfile_markdown.rewind
      @tempfile_markdown.close
      @tempfile_pdf = Tempfile.create(['pandoc-pdf-', '.pdf'])
      @tempfile_pdf.close
      command = %(
        pandoc --template=vendor/assets/doc-contract/pandoc-templates/eisvogel.tex
          --css=app/assets/stylesheets/doc_contract/pandoc.css #{@tempfile_markdown.path}
          -o #{@tempfile_pdf.path}
      ).strip.gsub(/\n\s*/, ' ')
      command << ' --variable top-level-division=chapter' if @record.config['book']
      #command << " --variable titlepage=true" if false # done through YAML config
      command << ' --listings' # if some conditions TODO: make this a database boolean
      command << ' --filter pandoc-latex-environment' if template_markdown['pandoc-latex-environment:']

      stdout, stderr, status = nil
      Dir.chdir DocContract::Engine.root do
        stdout, stderr, status = Open3.capture3(command)
      end

      if status.success?
        send_file @tempfile_pdf.path
      else
        redirect_back fallback_location: record_class, flash: {error: stderr}
      end
    ensure
      begin
        @tempfile_markdown.unlink
        @tempfile_pdf.unlink
      rescue
        nil
      end
    end
    # rubocop:enable Metrics/MethodLength

    private

    def record_class
      result = controller_path.classify.safe_constantize
      raise "Cannot determine record_class from controller_path: #{controller_path}" unless result

      result
    end

    def record_params
      params.require(record_class.name.demodulize.underscore).permit!
    end
  end
end
