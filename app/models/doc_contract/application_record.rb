# frozen_string_literal: true

module DocContract
  class ApplicationRecord < ActiveRecord::Base

    self.abstract_class = true

    RANSACKABLE_ATTRIBUTES = [].freeze
    RANSACKABLE_ASSOCIATIONS = [].freeze

    def self.ransackable_attributes(_auth_object = nil)
      if const_defined? :RANSACKABLE_ATTRIBUTES
        const_get :RANSACKABLE_ATTRIBUTES
      else
        RANSACKABLE_ATTRIBUTES
      end
    end

    def self.ransackable_associations(_auth_object = nil)
      if const_defined? :RANSACKABLE_ASSOCIATIONS
        const_get :RANSACKABLE_ASSOCIATIONS
      else
        RANSACKABLE_ASSOCIATIONS
      end
    end
  end
end
