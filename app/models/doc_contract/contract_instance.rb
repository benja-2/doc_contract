# frozen_string_literal: true

module DocContract
  class ContractInstance < ApplicationRecord

    belongs_to :contract_template

    validates :name, presence: true

    RANSACKABLE_ATTRIBUTES = %w[name].freeze
    RANSACKABLE_ASSOCIATIONS = %w[contract_template].freeze

    def config
      own_config = YAML.safe_load(config_yml.to_s, permitted_classes: [Symbol, Date]) || {}
      own_config.reverse_merge contract_template.config
    end

    def markdown
      contract_template.markdown
    end

    def markdown=(value)
      contract_template.markdown = value
    end

    def processed_markdown(config_addittions = {})
      config = self.config
      config.merge! config_addittions unless config_addittions.blank?
      DocContract::Handlebars.compile(markdown, config)
    end

    def template_markdown(config_addittions = {})
      config = self.config
      config.merge! config_addittions unless config_addittions.blank?
      result = DocContract::Handlebars.compile(markdown, config)
      # prefix the markdown with the config (eisvogel pandoc)
      "#{config.to_yaml}...\n\n#{result}"
    end

    def processed_html
      result = processed_markdown.sub(/.*\n...\n\n/m, '')
      markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, tables: true)
      markdown.render result
    end
  end
end
