# frozen_string_literal: true
module DocContract
  class ContractTemplate < ApplicationRecord

    HANDLEBARS_REGEX = /{{((#)?[a-z._ ]+)}}/

    RANSACKABLE_ATTRIBUTES = %w[name].freeze
    #RANSACKABLE_ASSOCIATIONS = %w[].freeze

    has_many :contract_instances, dependent: :delete_all

    validates :name, presence: true
    validates :config_yml, yaml: true

    def config
      YAML.safe_load(config_yml.to_s, permitted_classes: [Symbol, Date]) || {}
    end

    # Do not fail when an instance trait tries to access fallback template properties
    def contract_template
      self
    end

    def processed_markdown(config_addittions = {})
      config = self.config
      config.merge! config_addittions unless config_addittions.blank?
      DocContract::Handlebars.compile(markdown, config)
    end

    def template_markdown(config_addittions = {})
      config = self.config
      config.merge! config_addittions unless config_addittions.blank?
      result = DocContract::Handlebars.compile(markdown, config)
      # prefix the markdown with the config (eisvogel pandoc)
      "#{config.to_yaml}...\n\n#{result}"
    end

    def processed_html
      result = processed_markdown.sub(/.*\n...\n\n/m, '')
      markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, tables: true)
      markdown.render result
    end

    def present_handlebars_expressions
      markdown.to_s.scan(HANDLEBARS_REGEX).map(&:first).uniq
    end
  end
end
