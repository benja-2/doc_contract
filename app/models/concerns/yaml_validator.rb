# frozen_string_literal: true

class YamlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if value.blank?

    object = begin
      YAML.safe_load(value, permitted_classes: [Symbol, Date])
    rescue # Psych::SyntaxError
      nil
    end

    record.errors.add attribute, (options[:message] || 'is not valid YAML') unless object
  end
end
