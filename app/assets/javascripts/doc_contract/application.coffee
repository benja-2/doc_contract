#= require jquery
#= require jquery_ujs
#= require semantic-ui
#= require ace-rails-ap

latex_template_infos =
  eisvogel: '
    Look at: 
      <a href="https://github.com/Wandmalfarbe/pandoc-latex-template" target="_blank">
        https://github.com/Wandmalfarbe/pandoc-latex-template
      </a>'
$ ->
  $('.ui.dropdown').dropdown() # assume fomantic that has the clearable class option, a lot better than the below custom code
  $('.message .close').on 'click', ->
    $(@).closest('.message').transition('fade')

  $('.preview-markdown').click ->
    markdown = $('#contract_template_markdown').val()
    target = $(@).data('target')
    modal = $('#markdown-preview-modal')
    modal.modal 'show'
    $.post target, {markdown: markdown}, (response) ->
      content = modal.find('.content')
      content.html response.markdown
      false
  $('.preview-html').click ->
    markdown = $('#contract_template_markdown').val()
    target = $(@).data('target')
    modal = $('#html-preview-modal')
    modal.modal 'show'
    $.post target, {markdown: markdown}, (response) ->
      content = modal.find('.content')
      content.html response.html
      false

  latex_template = $('select#contract_template_latex_template')
  latex_template.change ->
    el = $(@)
    $('.contract_template_latex_template_info').remove()
    if info_html = latex_template_infos[el.val()]
      info = $('<button><i class="info icon"></button>').addClass('contract_template_latex_template_info circular ui olive icon button')
      info.popup
        html: info_html
        hoverable: true
      el.after info
  latex_template.change() if latex_template.length

  contract_config_yml = $('#contract_template_config_yml,#contract_instance_config_yml')
  if contract_config_yml.length
    config_yml_editor = ace.edit 'contract_config_yml_editor',
      mode: 'ace/mode/yaml'
      theme: 'ace/theme/monokai'
      value: contract_config_yml.val()
      tabSize: 2
      useSoftTabs: true
    config_yml_editor.getSession().on 'change', (something, session) ->
      contract_config_yml.val session.getValue()
      false
    contract_config_yml.hide()

  contract_markdown = $('#contract_template_markdown')
  if contract_markdown.length
    markdown_editor = ace.edit 'contract_template_markdown_editor',
      mode: 'ace/mode/markdown'
      theme: 'ace/theme/monokai'
      value: contract_markdown.val()
    markdown_editor.getSession().on 'change', (something, session) ->
      contract_markdown.val session.getValue()
      false
    contract_markdown.hide()
