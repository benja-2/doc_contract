# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'application/main.html.slim', type: :view do
  it 'renders the dummy page' do
    render
    expect(rendered).to match %r{href="/doc-contract/?"}
  end
end
