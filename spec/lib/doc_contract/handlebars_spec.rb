# frozen_string_literal: true

require 'rails_helper'

module DocContract
  RSpec.describe Handlebars do
    it 'Does basic compilation' do
      result = described_class.compile 'Let {{date}} be set', {date: '2022-02-18'}
      expect(result).to eq 'Let 2022-02-18 be set'
    end

    describe 'helper euro' do
      it 'supports euro presentation of floats' do
        result = described_class.compile 'My amount is {{euro amount}}', {amount: 2.24224}
        expect(result).to eq 'My amount is &euro; 2.24'
      end
      it 'supports euro presentation of large numbers' do
        result = described_class.compile 'My amount is {{euro amount}}', {amount: 2_352_525.24224}
        expect(result).to eq 'My amount is &euro; 2352525.24'
      end

      it 'supports euro presentation of integers' do
        result = described_class.compile 'My amount is {{euro amount}}', {amount: 5.00}
        expect(result).to eq 'My amount is &euro; 5,-'
      end
    end

    describe 'helper dollar' do
      it 'supports dollar presentation' do
        result = described_class.compile 'My amounts are {{dollar amount1}} and {{dollar amount2}}', {
          amount1: 2.24224,
          amount2: 5_500
        }
        expect(result).to eq 'My amounts are $ 2.24 and $ 5500,-'
      end
    end

    it 'can do plus operations' do
      result = described_class.compile 'My amount is {{plus amount1 amount2}}', {amount1: 2.24224, amount2: 4.2}
      expect(result).to eq 'My amount is 6.44224'
    end

    describe 'helper sum' do
      it 'can do sum operations using separate arguments' do
        result = described_class.compile 'My amount is {{sum a b c d}}', {a: 1, b: 2.2, c: nil, d: 4.2}
        expect(result).to eq 'My amount is 7.4'
      end

      it 'can do sum operations using array argument' do
        result = described_class.compile 'My amount is {{sum arry}}', {arry: [1, 2.2, nil, 4.2]}
        expect(result).to eq 'My amount is 7.4'
      end
    end

    it 'provides lists of items with amount of a total amount' do
      result = described_class.compile 'My amount is {{euro offer_items_total}} and {{grocery_items_total}}', {
        offer_items: [{amount: 2.1}, {amount: 5.2}, {amount: 5.3}],
        grocery_items: [{amount: 5}]
      }
      expect(result).to eq 'My amount is &euro; 12.60 and 5.0'
    end

    it 'can do total values using more complex nesting' do
      result = described_class.compile "My amount is {{euro (sum (map offer_items 'amount'))}}", {
        offer_items: [{amount: 2.1}, {amount: 5.2}, {amount: 5.3}]
      }
      expect(result).to eq 'My amount is &euro; 12.60'
    end

    it 'does markdown tables' do
      markdown = <<~MARKDOWNTABLE
        Onderdeel                   | Kosten/j
        ---                         | ---:
        {{#each offer_items}}{{this.label}} | {{euro this.amount}}
        {{/each}}**Totaal** | **{{euro offer_items_total}}**
      MARKDOWNTABLE
      result = described_class.compile markdown, {
        offer_items: [
          {label: 'Platform, Applicatie en API maintenance', amount: 2_500},
          {label: 'Data migration to v3', amount: 3_500}
        ]
      }

      expect(result).to eq <<~MARKDOWNTABLE
        Onderdeel                   | Kosten/j
        ---                         | ---:
        Platform, Applicatie en API maintenance | &euro; 2500,-
        Data migration to v3 | &euro; 3500,-
        **Totaal** | **&euro; 6000,-**
      MARKDOWNTABLE
    end

    it 'compiles certain config attributes' do
      markdown = <<~MARKDOWN
        {{title}}
        ==========
        Contract for {{customer}}
        as config for header-left we have {{contractor}}
      MARKDOWN
      result = described_class.compile markdown, {
        title: 'Onderhoudscontract {{customer}} {{year}}',
        year: 2022,
        customer: 'E-Corp',
        contractor: 'Me'
      }
      expect(result).to eq <<~RESULT
        Onderhoudscontract E-Corp 2022
        ==========
        Contract for E-Corp
        as config for header-left we have Me
      RESULT
    end

    it 'has a today handle' do
      result = described_class.compile 'It is {{today}}', nil
      expect(result).to eq "It is #{Date.today.iso8601}"
    end

    describe 'helper to_sentence' do
      it 'does to_sentence using a nester with symbol source' do
        result = described_class.compile "I am gonna buy: {{to_sentence grocery_items 'label'}}. Yeah!", {
          grocery_items: [
            {label: 'Apples', amount: 2.1},
            {label: 'Wheat', amount: 5.2},
            {label: 'Blueberries', amount: 5.3}
          ]
        }
        expect(result).to eq 'I am gonna buy: Apples, Wheat, and Blueberries. Yeah!'
      end

      it 'does to_sentence using a nester with string source' do
        result = described_class.compile "I am gonna buy: {{to_sentence grocery_items 'label'}}. Yeah!", {
          grocery_items: [
            {'label' => 'Apples', amount: 2.1},
            {'label' => 'Wheat', amount: 5.2},
            {'label' => 'Blueberries', amount: 5.3}
          ]
        }
        expect(result).to eq 'I am gonna buy: Apples, Wheat, and Blueberries. Yeah!'
      end

      it 'works with a plain array' do
        result = described_class.compile 'I am gonna buy: {{to_sentence groceries}}. Yeah!', {
          groceries: %w[Apples Wheat Blueberries]
        }
        expect(result).to eq 'I am gonna buy: Apples, Wheat, and Blueberries. Yeah!'
      end

      it 'Can be achieved using the nested map helper' do
        result = described_class.compile "I am gonna buy: {{to_sentence (map grocery_items 'label')}}. Yeah!", {
          grocery_items: [
            {'label' => 'Apples', amount: 2.1},
            {'label' => 'Wheat', amount: 5.2},
            {'label' => 'Blueberries', amount: 5.3}
          ]
        }
        expect(result).to eq 'I am gonna buy: Apples, Wheat, and Blueberries. Yeah!'
      end
    end

    describe 'helper to_words' do
      it 'converts number to words' do
        result = described_class.compile [
          "I am gonna buy: {{to_sentence grocery_items 'label'}}. ",
          'With a total of {{to_words grocery_items_total}} pesos'
        ].join, {
          grocery_items: [
            {'label' => 'Apples',      amount: 2_102_232},
            {'label' => 'Wheat',       amount: 3_245_102},
            {'label' => 'Blueberries', amount: 1e6}
          ]
        }
        expect(result).to eq(
          'I am gonna buy: Apples, Wheat, and Blueberries. ' \
          'With a total of six million three hundred forty-seven thousand three hundred thirty-four pesos'
        )
      end

      it 'converts number to words in Spanish ' do
        result = I18n.with_locale :es do
          described_class.compile [
            "Voy a comprar: {{to_sentence grocery_items 'label'}}. ",
            'With a total of {{to_words grocery_items_total}} pesos'
          ].join, {
            grocery_items: [
              {'label' => 'Apples',      amount: 2_102_232},
              {'label' => 'Wheat',       amount: 3_245_102},
              {'label' => 'Blueberries', amount: 1e6}
            ]
          }
        end
        expect(result).to eq(
          'Voy a comprar: Apples, Wheat y Blueberries. ' \
          'With a total of seis millones trescientos cuarenta y siete mil trescientos treinta y cuatro pesos'
        )
      end
    end

    describe 'helper upcase' do
      it 'converts to uppercase' do
        result = Handlebars.compile 'Hello my name in capitals is: {{upcase name}}. Capiche?', {
          name: 'Benjamin'
        }
        expect(result).to eq 'Hello my name in capitals is: BENJAMIN. Capiche?'
      end
    end

    describe 'helper downcase' do
      it 'converts to uppercase' do
        result = Handlebars.compile 'Hello my name in capitals is: {{downcase name}}. Capiche?', {
          name: 'Benjamin'
        }
        expect(result).to eq 'Hello my name in capitals is: benjamin. Capiche?'
      end
    end

    describe 'helper eq' do
      it 'works with equality' do
        result = Handlebars.compile "My result is: {{#if (eq my_action 'upcase')}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase'
        }
        expect(result).to eq 'My result is: UPCASE'
      end

      it 'works with multiple equality' do
        result = Handlebars.compile "My result is: {{#if (eq my_action 'upcase' my_other_action)}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: UPCASE'
      end

      it 'works with equality not equal' do
        result = Handlebars.compile "My result is: {{#if (eq my_action 'upcase')}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase-not'
        }
        expect(result).to eq 'My result is: no upcase'
      end

      it 'works with multiple equality not equal' do
        result = Handlebars.compile "My result is: {{#if (eq my_action 'upcase2' my_other_action)}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: no upcase'
      end

      it 'works as a block helper' do
        result = Handlebars.compile "My result is: {{#eq my_action 'upcase' my_other_action}}UPCASE{{else}}no upcase{{/eq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: UPCASE'
      end

      it 'works as a block helper without else and true' do
        result = Handlebars.compile "My result is: {{#eq my_action 'upcase' my_other_action}}UPCASE{{/eq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: UPCASE'
      end

      it 'works as a block helper without else and false' do
        result = Handlebars.compile "My result is: {{#eq my_action 'upcase2' my_other_action}}UPCASE{{/eq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: '
      end

      it 'works as a block helper with else and false' do
        result = Handlebars.compile "My result is: {{#eq my_action 'upcase2' my_other_action}}UPCASE{{else}}no upcase block{{/eq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: no upcase block'
      end
    end

    describe 'helper neq' do
      it 'works with non-equality' do
        result = Handlebars.compile "My result is: {{#if (neq my_action 'upcase')}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase'
        }
        expect(result).to eq 'My result is: no upcase'
      end

      it 'works with multiple non-equality' do
        result = Handlebars.compile "My result is: {{#if (neq my_action 'upcase' my_other_action)}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: no upcase'
      end

      it 'works with non-equality not equal' do
        result = Handlebars.compile "My result is: {{#if (neq my_action 'upcase')}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase-not'
        }
        expect(result).to eq 'My result is: UPCASE'
      end

      it 'works with multiple non-equality not equal' do
        result = Handlebars.compile "My result is: {{#if (neq my_action 'upcase2' my_other_action)}}UPCASE{{else}}no upcase{{/if}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: UPCASE'
      end

      it 'works as a block helper' do
        result = Handlebars.compile "My result is: {{#neq my_action 'upcase' my_other_action}}UPCASE{{else}}no upcase{{/neq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: no upcase'
      end

      it 'works as a block helper without else and true' do
        result = Handlebars.compile "My result is: {{#neq my_action 'upcase' my_other_action}}UPCASE{{/neq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: '
      end

      it 'works as a block helper without else and false' do
        result = Handlebars.compile "My result is: {{#neq my_action 'upcase2' my_other_action}}UPCASE{{/neq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: UPCASE'
      end

      it 'works as a block helper with else and false' do
        result = Handlebars.compile "My result is: {{#neq my_action 'upcase2' my_other_action}}UPCASE{{else}}no upcase block{{/neq}}", {
          my_action: 'upcase',
          my_other_action: 'upcase'
        }
        expect(result).to eq 'My result is: UPCASE'
      end
    end

    describe 'helper includes' do
      it 'works as arguments based' do
        result = Handlebars.compile "{{#if (includes (map items 'label') 'Aap')}}Yes Aap!{{else}}No Aap{{/if}}", {
          items: [{label: 'Aap'}, {label: 'Noot'}, {label: 'Nies'}]
        }
        expect(result).to eq 'Yes Aap!'
      end

      it 'works as arguments based (negated)' do
        result = Handlebars.compile "{{#if (includes (map items 'label') 'Aap')}}Yes Aap!{{else}}No Aap{{/if}}", {
          items: [{label: 'Chimp'}, {label: 'Noot'}, {label: 'Nies'}]
        }
        expect(result).to eq 'No Aap'
      end
      it 'works block based' do
        result = Handlebars.compile "{{#includes items 'Aap'}}Yes Aap!{{else}}No Aap{{/includes}}", {
          items: %w[Aap Noot Nies]
        }
        expect(result).to eq 'Yes Aap!'
      end
    end

    describe 'helper gt' do
      it 'works block based' do
        result = Handlebars.compile '{{#gt a 7}}Yes Aap!{{else}}No Aap{{/gt}}', {
          a: 8
        }
        expect(result).to eq 'Yes Aap!'
      end

      it 'works block based negated' do
        result = Handlebars.compile '{{#gt a 709}}Yes Aap!{{else}}No Aap{{/gt}}', {
          a: 6
        }
        expect(result).to eq 'No Aap'
      end
    end

    describe 'helper first_present' do
      it 'gives first present option' do
        result = Handlebars.compile "{{first_present a b 'c'}}", {
          a: 6
        }
        expect(result).to eq '6'
      end

      it 'gives first present option when first option is not given' do
        result = Handlebars.compile "{{first_present a b 'c'}}", {
          b: 7
        }
        expect(result).to eq '7'
      end

      it 'allows inline default params' do
        result = Handlebars.compile "{{first_present a b 'c'}}", {
          c: 7
        }
        expect(result).to eq 'c'
      end
    end
  end
end
