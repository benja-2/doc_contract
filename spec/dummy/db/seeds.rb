# frozen_string_literal: true

unless User.find_by(email: 'admin@example.com').present?
  puts 'Creating admin'
  User.create!(email: 'admin@example.com', password: 'admin123')
end
