# frozen_string_literal: true
require 'rails_helper'

module DocContract
  RSpec.describe ApplicationHelper, type: :helper do
    describe 'contract_instances controller' do
      let(:helper_controller) { ContractInstancesController.new.tap{ _1.action_name = 'show' } }
      before do
        allow(helper).to receive(:controller) { helper_controller }
      end

      describe 'active_class' do
        it 'is active for the namespace' do
          helper.active_class('doc_contract').should eq 'active'
        end
        it 'is active for the controller' do
          helper.active_class('doc_contract/contract_instances').should eq 'active'
        end
        it 'is not active for another controller' do
          helper.active_class('doc_contract/contract_instances').should eq 'active'
        end
        it 'matches the action when needed' do
          helper.active_class('doc_contract/contract_instances#show').should eq 'active'
        end
        it 'does not match the action when needed' do
          helper.active_class('doc_contract/contract_instances#index').should be_nil
        end
        it 'works using a regexp' do
          helper.active_class(%r{doc_contract/contract_(instances|templates)#(show|index)}).should eq 'active'
        end
      end
    end
  end
end
