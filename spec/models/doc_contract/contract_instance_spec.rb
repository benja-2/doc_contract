# frozen_string_literal: true

require 'rails_helper'

module DocContract
  RSpec.describe ContractInstance, type: :model do
    let(:markdown) { "# Intoduction\n## With subtitle\n\nAnd content" }
    let(:contract_template) do
      ContractTemplate.create(
        name: 'Test Template',
        markdown: markdown,
        config_yml: <<~YML
          title: My lazy maintenance
          customer: E-Corp
          header-left: '{{customer}}'
          my-date: 2022-09-21
        YML
      )
    end
    subject { ContractInstance.new name: 'Test instance', contract_template: contract_template }

    describe :config do
      it 'returns the template config if empty' do
        expect(subject.config).to eq \
          'title'       => 'My lazy maintenance',
          'customer'    => 'E-Corp',
          'header-left' => '{{customer}}',
          'my-date'     => '2022-09-21'.to_date
      end

      it 'prefers own config and enriches' do
        subject.config_yml = <<~YML
          header-left: My dear {{customer}}
          year: 2022
        YML

        expect(subject.config).to eq \
          'title'       => 'My lazy maintenance',
          'customer'    => 'E-Corp',
          'header-left' => 'My dear {{customer}}',
          'my-date'     => '2022-09-21'.to_date,
          'year'        => 2022
      end
    end

    describe :template_markdown do
      let(:markdown) do
        <<~MARKDOWN
          # Introduction
          ## Client
          We have date: {{my-date}}
        MARKDOWN
      end

      it 'correctly parses the date' do
        expect(subject.template_markdown).to include 'We have date: 2022-09-21'
      end
    end
  end
end
