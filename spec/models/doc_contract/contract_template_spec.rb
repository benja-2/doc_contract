# frozen_string_literal: true

require 'rails_helper'

module DocContract
  RSpec.describe ContractTemplate, type: :model do
    let(:markdown) do
      <<~MARKDOWN
        # Introduction
        ## Client

        This maintenance contract is requested by:
        {{full_customer_name}}, hereafter {{customer}},

        And created by:
        {{full_contractor_name}}, hereafter {{contractor}}.


        ## Behest
        contract stuff

        \newpage
        # Quote breakdown

        ## Fixed costs

        Subject                     | Cost/y
        ---                         | ---:
        {{#each offer_items}}{{this.label}} | {{euro this.amount}}
        {{/each}}**Total** | **{{euro offer_items_total}}**

        ## Variable costs
        And more contract stuff
      MARKDOWN
    end
    subject { ContractTemplate.new markdown: markdown }
    describe :present_handlebars_expressions do
      it 'finds the statements' do
        expect(subject.present_handlebars_expressions).to eq [
          'full_customer_name',
          'customer',
          'full_contractor_name',
          'contractor',
          '#each offer_items',
          'this.label',
          'euro this.amount',
          'euro offer_items_total'
        ]
      end
    end
  end
end
