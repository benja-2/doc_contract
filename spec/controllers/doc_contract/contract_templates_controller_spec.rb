# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DocContract::ContractTemplatesController, type: :controller do
  routes { DocContract::Engine.routes }
  let!(:user) { User.create email: 'template-user@example.com', password: 'admin123' }
  describe 'GET index' do
    describe 'signed in' do
      before { sign_in user }

      it 'returns a 200' do
        get :index
        expect(response).to have_http_status(:ok)
      end
    end

    it 'redirects to sign_in' do
      get :index
      expect(response).to redirect_to '/users/sign_in'
    end
  end
end
