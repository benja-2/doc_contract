# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'DocContract::ContractTemplatesController', type: :request do
  #before(:each) { helper.class.include DocContract::Engine.routes.url_helpers }

  let(:template_yaml) do
    <<~'TEMPLATE_YML'
      titlepage:          true
      title:              Maintenance contract {{customer}} {{year}}
      subtitle:           To the future and beyond!
      date:               2022-01-24
      year:               2021
      full_customer_name: E-corp located in Gotham City
      customer:           E-corp
      contractor:         Path-E-Tech Management
      full_contractor_name: Path-E-Tech Management L.L.C. located in Gotham City
      production_url:     https://contracts.path-e-tech.com
      staging_url:        https://contracts-staging.path-e-tech.com
      support_email:      support@path-e-tech.com
      author:             Dullbert
      header-left:        "{{title}}"
      header-right:       "{{contractor}}"
      footer-left:        "{{customer}} | {{today}}"
      toc:                true
      toc-own-page:       true
      numbersections:     true
      footer-right:       '\thepage\ of \pageref{LastPage}'
      geometry:           margin=2.5cm
      urlcolor:           blue
      header-includes:    '\usepackage{lastpage}'
      offer_items:
        - label: Maintenance of the platform infrastructure
          amount: 2_000
        - label: Maintenance of the application API
          amount: 2_000
        - label: Do migrations for the Infrastructure adapter
          amount: 2_500
    TEMPLATE_YML
  end

  let(:markdown) do
    <<~'MARKDOWN'
      # Introduction

      ## Client

      This maintenance contract is requested by:
      {{full_customer_name}}, hereafter {{customer}},

      And created by:
      {{full_contractor_name}}, hereafter {{contractor}}.


      ## Behest
      contract stuff

      \newpage
      # Quote breakdown

      ## Fixed costs

      Subject                     | Cost/y
      ---                         | ---:
      {{#each offer_items}}{{this.label}} | {{euro this.amount}}
      {{/each}}**Total** | **{{euro offer_items_total}}**

      The total costs for {{year}} for {{to_sentence (map offer_items 'label')}} will thereby be {{to_words offer_items_total}} ({{euro offer_items_total}}). All the amounts include
      a VAT of 21%.

      ## Variable costs
      And more contract stuff
    MARKDOWN
  end
  let(:template) { DocContract::ContractTemplate.create! name: 'I am', config_yml: template_yaml, markdown: }
  let(:user)     { User.new }

  describe 'signed in' do
    before { sign_in user }

    describe 'show' do
      it 'renders the page' do
        get polymorphic_path([doc_contract, template])
        expect(response.status).to be 200
      end
    end

    describe 'pdf' do
      it 'fetches the pdf' do
        get polymorphic_path([doc_contract, template], action: :pdf)
        expect(response.body).to start_with '%PDF-1.5'
      end

      it 'redirects without markdown' do
        template.update(markdown: nil)
        get polymorphic_path([doc_contract, template], action: :pdf)
        expect(response).to redirect_to "/doc-contract/contract_templates/#{template.id}"
      end
    end
  end
end
