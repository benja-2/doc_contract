# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Applications', type: :request do
  describe 'GET /main' do
    it 'returns http success' do
      get '/'
      expect(response).to redirect_to '/users/sign_in'
    end
  end
end
