Changelog
=================

### 0.2.0 (2023-08-07)
* Refactors to adhere to rubocop styleguide
* Better form errors
* More specs for rendering

### 0.1.6 (2022-09-xx)
* Use explicit safe\_load for YAML loading for compatibility with ruby versions
* Add name sorting for listings

### 0.1.5 (2022-09-17)
* Add explicit require statements for controller specs

### 0.1.4 (2022-09-17)
* Add explicit ransack for more explicit rails7/ransack requirements

### 0.1.3 (2022-09-17)
* Add CHANGELOG
* Make specs Ruby 3 compatible
* README additions

## 0.1.0
Placeholder version for sub version 1 upgrades.

