# frozen_string_literal: true
require_relative 'lib/doc_contract/version'

Gem::Specification.new do |spec|
  spec.name        = 'doc_contract'
  spec.version     = DocContract::VERSION
  spec.authors     = ['Benjamin ter Kuile']
  spec.email       = ['bterkuile@gmail.com']
  spec.homepage    = 'https://gitlab.com/benja-2/doc_contract'
  spec.summary     = 'Create nice and easy contracts based on pandoc'
  spec.description = "This is a mountable rails engine to create nice and easy contracts based on pandoc (which uses
                      LaTeX) that includes the GUI and can be used to include the relevant contracts to the system itself
                      or to simply have a WebGUI to make contract templates and instances"
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  #spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  #spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  #spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir['{app,config,db,lib,vendor}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  end

  spec.add_dependency 'cancancan'
  spec.add_dependency 'jquery-rails'
  spec.add_dependency 'kaminari'
  spec.add_dependency 'numbers_and_words'
  spec.add_dependency 'rails' #, ">= 7.0.1"
  spec.add_dependency 'ransack'
  spec.add_dependency 'redcarpet'
  spec.add_dependency 'ruby-handlebars'

  spec.add_development_dependency 'coffee-rails'
  spec.add_development_dependency 'factory_bot_rails'
  spec.add_development_dependency 'rspec-rails'
end
